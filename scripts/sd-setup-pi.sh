#!/bin/sh
# Run this on the raspberry PI

set -ex

sudo apt update

# Change hostname
sudo cp /etc/hostname /etc/hostname.bak
sudo bash -c 'echo "clickr" > /etc/hostname'
sudo sed -i'.bak' -e 's/raspberrypi/clickr/g' /etc/hosts

# Install home_assistant
sudo apt install --yes network-manager apparmor jq
curl https://get.docker.com | sudo bash
sudo usermod -aG docker $USER
newgrp docker
curl -o hass-installer.sh https://raw.githubusercontent.com/home-assistant/supervised-installer/master/installer.sh
sudo bash ./hass-installer.sh --machine raspberrypi4

# Configure 3.5" display
git clone https://github.com/waveshare/LCD-show.git
cd LCD-show/
chmod +x LCD35-show
cd ../

# Fix touch input
sudo apt --yes install xserver-xorg-input-evdev
sudo sed -i'.bak' -e 's/Driver "libinput"/Driver "evdev"/g' /usr/share/X11/xorg.conf.d/40-libinput.conf
echo 'Section "InputClass"
        Identifier      "calibration"
        MatchProduct    "ADS7846 Touchscreen"
        Option  "MinX"  "4551"
        Option  "MaxX"  "62076"
        Option  "MinY"  "62002"
        Option  "MaxY"  "3430"
        Option  "SwapXY"        "0" # unless it was already set to 1
        Option  "InvertX"       "0"  # unless it was already set
        Option  "InvertY"       "0"  # unless it was already set
        Option "TransformationMatrix" "0 -1 1 1 0 0 0 0 1"
EndSection' | sudo tee -a /usr/share/X11/xorg.conf.d/99-calibration.conf

# Configure kiosk mode
sudo apt --yes install xdotool unclutter sed
echo '#!/bin/bash
xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences

/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk https://localhost:8123
' | sudo tee -a kiosk.sh
echo '[Unit]
Description=Chromium Kiosk
Wants=graphical.target
After=graphical.target

[Service]
Environment=DISPLAY=:0.0
Environment=XAUTHORITY=/home/pi/.Xauthority
Type=simple
ExecStart=/bin/bash /home/pi/kiosk.sh
Restart=on-abort
User=pi
Group=pi

[Install]
WantedBy=graphical.target' | sudo tee -a /lib/systemd/system/kiosk.service
sudo systemctl enable kiosk.service


# Manual steps
echo "Configure auto login: sudo raspi-config"
echo "Configure wifi: sudo raspi-config"
echo "Restarting now"
sleep 10s

# Enable display and reboot
 ./LCD-Show/LCD35-show
