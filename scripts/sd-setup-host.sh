#!/bin/sh
# Run: ./sd-setup-host.sh </path/to/pi/boot/partition>
# Run this on your computer, with the raspberry pi SD card mounted

BOOT_PARTITION=$1

cd $BOOT_PARTITION

set -ex

touch ssh

cp config.txt config.txt.bak
echo "dtoverlay=dwc2" >> config.txt

sed -i'.bak' -e 's/rootwait/rootwait modules-load=dwc2,g_ether/g' cmdline.txt
