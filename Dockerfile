ARG BUILD_FROM

# Docker context: clickr_umbrella root

# Builder
FROM $BUILD_FROM AS builder

RUN apk add --no-cache elixir
RUN mix local.hex --force
RUN mix local.rebar --force

COPY . ./
COPY mix.exs ./
COPY mix.lock ./
RUN mix deps.get
RUN mix deps.compile

COPY config/ ./
COPY lib/ ./

RUN MIX_ENV=prod mix release


# Runner
FROM $BUILD_FROM
ENV LANG C.UTF-8
WORKDIR /addon

RUN apk add --no-cache elixir

COPY --from=builder _build/prod/rel/clickr_home_assistant/ ./release

CMD [ "./release/bin/clickr_home_assistant", "start" ]
