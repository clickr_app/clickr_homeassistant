defmodule ClickrHomeAssistant.MixProject do
  use Mix.Project

  def project do
    [
      app: :clickr_home_assistant,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {ClickrHomeAssistant.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}

      {:websockex, "~> 0.4.3"},
      {:jason, "~> 1.2.2"}
    ]
  end
end
