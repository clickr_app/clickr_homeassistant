import Config

config :clickr_home_assistant,
  local_url: System.get_env("HOMEASSISTANT_WS_URL", "ws://supervisor/core/websocket"),
  local_auth_token:
    System.get_env("HOMEASSISTANT_AUTH_TOKEN") || System.get_env("SUPERVISOR_TOKEN"),
  remote_url: System.get_env("CLICKR_WS_URL", "ws://clickr.ftes.de/api/websocket"),
  remote_auth_token: System.get_env("CLICKR_AUTH_TOKEN")

config :logger, level: String.to_atom(System.get_env("CLICKR_LOG_LEVEL", "debug"))
