defmodule ClickrHomeAssistant.RemoteAdapter do
  use ClickrHomeAssistant.JsonAdapter
  require Logger

  alias ClickrHomeAssistant.{JsonAdapter, LocalAdapter}

  @heartbeat_ms 30_000

  # Client API
  def start_link(_opts) do
    auth_token = Application.fetch_env!(:clickr_home_assistant, :remote_auth_token)
    url = Application.fetch_env!(:clickr_home_assistant, :remote_url)
    url = "#{url}?auth_token=#{auth_token}"

    WebSockex.start_link(url, __MODULE__, :initial,
      name: __MODULE__,
      handle_initial_conn_failure: true
    )
  end

  def send(type, msg) do
    WebSockex.cast(__MODULE__, {:send, type, msg})
  end

  # Callbacks (server API)
  @impl WebSockex
  def handle_connect(_conn, :initial) do
    Logger.info("connected")

    # work around: can't reply in handle_connect, and can't call WebSockex.send_frame (CallingSelfError)
    WebSockex.cast(self(), :join_channel_on_connect)
    Process.send_after(self(), :send_heartbeat, @heartbeat_ms)
    {:ok, {:wait_to_join, 1}}
  end

  @impl true
  def handle_info(:send_heartbeat, {status, ref}) do
    Logger.debug("heartbeat")
    Process.send_after(self(), :send_heartbeat, @heartbeat_ms)

    reply(
      %{event: "heartbeat", topic: "phoenix", payload: %{}, ref: ref},
      {status, ref + 1}
    )
  end

  @impl WebSockex
  def handle_cast(:join_channel_on_connect, {:wait_to_join, ref}) do
    Logger.debug("join")

    reply(
      %{event: "phx_join", topic: "home_assistant", payload: %{}, ref: ref},
      {:joining, ref + 1}
    )
  end

  def handle_cast({:send, type, msg}, {:joined, ref}) do
    Logger.debug("send #{inspect type}")
    reply(
      %{
        topic: "home_assistant",
        event: type,
        payload: msg,
        ref: ref
      },
      {:joined, ref + 1}
    )
  end

  @impl JsonAdapter
  def handle_json(
        %{"event" => "phx_reply", "payload" => %{"status" => "ok"}, "ref" => reply_ref},
        {:joining, ref}
      ) when reply_ref == ref - 1 do
    Logger.debug("joined")
    {:ok, {:joined, ref}}
  end

  def handle_json(
        %{"event" => "phx_reply", "payload" => %{"status" => "ok"}, "ref" => reply_ref},
        {status, ref}
      ) when reply_ref == ref - 1 do
    {:ok, {status, ref}}
  end

  def handle_json(
        %{"event" => "phx_error"} = msg,
        state
      ) do
    Logger.error("remote error: #{inspect msg}")
    {:ok, state}
  end

  def handle_json(%{type: "forward", payload: payload}, {:joined, ref}) do
    Logger.debug("forward payload")
    LocalAdapter.send(payload)
    {:ok, {:joined, ref}}
  end

  @impl WebSockex
  def handle_disconnect(%{reason: %{code: 403}}, :initial) do
    Logger.error("disconnected due to 403 forbidden -> stop")
    {:ok, :auth_invalid}
  end

  @impl WebSockex
  def handle_disconnect(%{reason: reason}, _state) do
    Logger.error("disconnected -> reconnect (reason: #{inspect(reason)}")
    Process.sleep(1000)
    {:reconnect, :initial}
  end
end
