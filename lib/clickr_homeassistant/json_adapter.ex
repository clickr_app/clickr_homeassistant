defmodule ClickrHomeAssistant.JsonAdapter do
  @callback handle_json(map(), state :: term) ::
              {:ok, new_state}
              | {:reply, WebSockex.frame(), new_state}
              | {:close, new_state}
              | {:close, WebSockex.close_frame(), new_state}
            when new_state: term

  def send(client, msg) do
    WebSockex.send_frame(client, {:text, Jason.encode!(msg)})
  end

  defmacro __using__(_opts) do
    quote do
      use WebSockex
      require Logger

      @behaviour ClickrHomeAssistant.JsonAdapter

      @impl WebSockex
      def handle_frame({:text, msg}, state) do
        decoded = Jason.decode!(msg)

        try do
          case handle_json(decoded, state) do
            {:reply, msg, state} -> reply(msg, state)
            other -> other
          end
        rescue
          error ->
            Logger.error(
              "Failed to handle #{inspect(msg)} in #{inspect(state)} (error: #{inspect(error)})"
            )

            {:ok, state}
        end
      end

      def handle_frame({type, msg}, state) do
        Logger.info("Unhandled Message - Type: #{inspect(type)} -- Message: #{inspect(msg)}")
        {:ok, state}
      end

      defp reply(msg, state) do
        {:reply, {:text, Jason.encode!(msg)}, state}
      end
    end
  end
end
