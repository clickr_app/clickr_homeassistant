# ClickrHomeAssistant

## Configuration

Environment variables:

| name | default | description |
|------|---------|-------------|
| `CLICKR_LOG_LEVEL` | `info` | Set the log level |
